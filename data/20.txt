My youngest kids are at the age where they like to smack toy balls around the house for giggles. One ball in particular is a giant cat toy yarn ball with a bell inside. This specific ball is my 7 month olds favorite.

While playing with her yarn ball on the floor I notice it starts to unravel. Being the "on top of it" paranoid about everything dad that I am I grab the ball and start trying to figure out how to fix it. The only fast logical solution I can think of is trim the yarn that came off and glue down the end.

I search for glue and in the end I locate a tube of superglue. No problem. I'll just put a dot where the yarn end is and let it dry.

I puncture the tube ever so gently to squeeze a drop onto the end of the loose yarn.... cap pops off and half the tube is now on the yarn. Whatever, even more secure. I set it on the kitchen counter to dry and go check on the kids.

About 60 seconds later a smoke alarm starts blaring and I run around the house trying to find which alarm is going off... it's the one in the kitchen. Yarn ball is smoking like crazy and it's freaking on fire. WTF?!

Come to find out with enough superglue and fabric the combo can get so hot it ignites. So now the balls extra safe because it's outside in the dumpster.

TL:DR

Put superglue on a ball of yarn trying to fix it. It caught fire. Ruined daughters favorite ball. I need a chemistry refresh.