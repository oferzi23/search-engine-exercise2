# search-engine-exercise2

## Installation

1. install dependencies
2. cd into project dir
3. inside [start.py](../blob/master/start.py) on line 12 you set the path to the dataset directory.
   
> Example datasets are at `./data`

4. inside [start.py](../blob/master/start.py) on line 13 you set language of the dataset.
5. inside [start.py](../blob/master/start.py) on line 14 you set the block size.
5. to start run `python start.py`

## Dependencies

- install python >= 3.5
- install python dependencies using `pip install -r requirements.txt`
- Before first run you have to download nltk data to use in this script 


> ## **NLTK data download**
> ### __Windows__ 
>   run `python -c 'import nltk; nltk.download()'`
> ### __Mac / Linux / Unix__
>   run `python -m nltk.downloader all`