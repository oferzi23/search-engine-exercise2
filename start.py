# ================================ imports ================================
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from os import path
import string
import csv

# ================================ General Variables ================================

FILES_PATH = './data/'
LANGUAGE = 'english'
BLOCK_SIZE = 10

MERGED_FILE = 'merged_block.csv'

normalize_values = {
    '0': 'zero',
    '1': 'one',
    '2': 'two',
    '3': 'three',
    '4': 'four',
    '5': 'five',
    '6': 'six',
    '7': 'seven',
    '8': 'eight',
    '9': 'nine',
    '10': 'ten',
    '$': 'dollar',
    '%': 'percent',
    '&': '',
    '#': 'hashtag'
}


# ================================ Functions ================================

def my_tokenization_function(document_text):
    document_text = document_text.encode('utf-8')
    document_text = document_text.decode('unicode-escape')
    stop_words = set(stopwords.words('english') + list(string.punctuation))
    ps = PorterStemmer()

    # Tokenize
    terms = word_tokenize(document_text)

    # Normalize
    terms = [term if term not in normalize_values else normalize_values[term] for term in terms]

    # Remove stops
    terms = [term for term in terms if term not in stop_words]

    # Stem
    terms = [ps.stem(term.lower()) for term in terms]

    return terms


class BlockStreamer:
    '''Streamer calss for iteration on blocks'''

    def __init__(self, file_path, p_size):
        self.current_line = 0
        self.path = file_path
        self.p_size = p_size

    def next(self):
        '''get block next section'''

        res = []

        with open(self.path, 'r') as f:

            reader = csv.reader(f, delimiter=',')

            line_seek_end = self.current_line + self.p_size

            num_row = len(f.readlines())

            if line_seek_end > num_row:
                line_seek_end = num_row

            f.seek(self.current_line)

            for index, line in enumerate(reader):

                if (line_seek_end == self.current_line):
                    break
                res.append((line[0], line[1]))

                self.current_line += 1

        return res

    def has_more_sections(self):
        '''Check if the block has more lines. in a csv file, EOF is marked with'''

        with open(self.path, 'r') as f:
            f.seek(self.current_line)
            if (f.readline() == ''):
                return False
        return True


class IREngine(object):
    def __init__(self, block_size):
        self.documents = {}
        self.inverted_index = {}
        self.block_size = block_size

        self.blocks = []
        self.current_block_postings = []
        self.current_block = 1

    def index(self, idx, document_text, tokenization_function):
        self.documents[idx] = document_text
        terms = tokenization_function(document_text)

        found = {}

        for term in terms:
            if term not in found:
                found[term] = True

                if len(self.current_block_postings) == self.block_size:
                    self.current_block_postings.sort(key=lambda x: x[0])

                    block_path = 'blockstore/block' + str(self.current_block) + '.csv'
                    with open(block_path, 'w', newline='') as block:
                        csv_out = csv.writer(block, )
                        for row in self.current_block_postings:
                            csv_out.writerow(row)

                    self.blocks.append(block_path)

                    self.current_block_postings = []
                    self.current_block += 1

                self.current_block_postings.append((term, idx))

    def merge(self):

        if not path.isfile(MERGED_FILE):

            def sections_are_done(secs, pts):

                def section_is_done(sec, pt):
                    if (pt == len(sec)):
                        return True
                    return False

                res = False
                for idx, sec in enumerate(secs):
                    if section_is_done(sec, pts[idx]):
                        res = True
                    else:
                        return False
                return res

            sec_size = 10

            streams = [None] * len(self.blocks)

            for index, block in enumerate(self.blocks):
                stream = BlockStreamer(block, sec_size)
                if (stream.has_more_sections()):
                    streams[index] = stream

            streams = [stream for stream in streams if stream is not None]

            blocks_are_not_empty = True
            while (blocks_are_not_empty):

                sections = [None] * len(streams)

                for index, stream in enumerate(streams):
                    sections[index] = stream.next()

                pointers = [0] * len(sections)

                merged_section = []

                while (not sections_are_done(sections, pointers)):

                    lines = [None] * len(sections)
                    for index, section in enumerate(sections):
                        if (pointers[index] < len(section)):
                            lines[index] = section[pointers[index]]

                    minimal = (0, lines[0])
                    for ind, obj in enumerate(lines):

                        if (minimal[1] is None or (obj is not None and (obj[0] < minimal[1][0]))):
                            minimal = (ind, obj)

                    merged_section.append(minimal[1])
                    pointers[minimal[0]] += 1

                with open(MERGED_FILE, 'a+', newline='') as f:
                    csv_out = csv.writer(f)
                    for line in merged_section:
                        csv_out.writerow(line)

                streams = [stream for stream in streams if stream.has_more_sections()]
                sections = [None] * len(streams)
                for index, stream in enumerate(streams):
                    sections[index] = stream.next()
                pointers = [0] * len(sections)

                blocks_are_not_empty = len(streams) == 0

        self.__rebuild_index()

    def __rebuild_index(self):
        with open(MERGED_FILE, 'r') as f:
            reader = csv.reader(f, delimiter=',')
            for line in reader:
                term = line[0]
                idx = int(line[1])
                if term in self.inverted_index and idx not in self.inverted_index[term]:
                    self.inverted_index[term].append(idx)
                else:
                    self.inverted_index[term] = [idx]

    # implement and query between 2 terms
    def intersection(self, term1, term2):

        if term1 in self.inverted_index and term2 in self.inverted_index:
            i = 0
            j = 0
            res = []
            lst1 = self.inverted_index[term1]
            lst2 = self.inverted_index[term2]

            lst1.sort
            lst2.sort


            shortest = lst1 if len(lst1) <= len(lst2) else lst2
            longest = lst2 if len(lst1) <= len(lst2) else lst1

            while (i < len(shortest) and j < len(longest)):
                if shortest[i] == longest[j]:
                    res.append(shortest[i])
                    i += 1
                    j += 1

                elif shortest[i] > longest[j]:
                    j += 1

                else:
                    i += 1

            return res

        else:
            return []

    # implement or query between 2 terms
    def union(self, term1, term2):
        lst1 = []
        lst2 = []

        if term1 in self.inverted_index:
            lst1 = self.inverted_index[term1]

        if term2 in self.inverted_index:
            lst2 = self.inverted_index[term2]

        return list(set(lst1 + lst2))

    def get_document_by_index(self, idx):
        return self.documents[idx]


def get_search_query():
    '''get user input search string'''

    return input("Please enter search query: ")


def get_search_type():
    ''' get user search type'''

    ans_valid = False
    while (not ans_valid):
        t = input(
            '''please select the type of search you would like to run:\n0. print inverted index\n1. intersection\n2. union\n enter selection: ''')

        if t not in ('0', '1', '2'):
            print("ERROR: invalid value in answer please enter selected number [ 0, 1, 2 ]")
        else:
            return t


def main():
    engine = IREngine(BLOCK_SIZE)

    # files = [FILES_PATH + '/1.txt']

    for index in range(1,21):
        with open(FILES_PATH + str(index) + '.txt', 'r') as f:
            text = f.read()
            engine.index(index, text, my_tokenization_function)

    engine.merge()

    # user input loop
    cont = True
    while (cont):

        # select search type
        q_type = get_search_type()

        if (int(q_type) == 0):
            print(engine.inverted_index)
        else:
            #  get search query from user
            query = get_search_query()

            #  get matching files index
            tokens = my_tokenization_function(query)

            if len(tokens) > 1:
                if (int(q_type) == 1):
                    res = engine.intersection(tokens[0], tokens[1])
                else:
                    res = engine.union(tokens[0], tokens[1])
                print(res)
                print('results:  {}'.format(
                    ["index: {0}, file: {1}".format(id, engine.get_document_by_index(id)) for id in res]))
            else:
                print("Error: invalid query params, some tokens might have been cut off as stop words")


        # validate user input
        ans_valid = False
        while (not ans_valid):
            ans = input("run again? (y/n): ")
            if (ans.lower() == 'n'):
                ans_valid = True
                cont = False
            elif (ans.lower() == 'y'):
                ans_valid = True
            else:
                print("ERROR: invalid value in answer please enter y / n")
    return


if __name__ == '__main__':
    main()